<?php

function hook_field_widget_form_control_info() {
  return array('hide' => array(
      'title' => t('Hide'),
      'element' => '#disabled',
      'value' => TRUE,
    ),
  );
}

function hook_field_widget_form_control_info_alter(&$settings) {
  if ($settings['hide']) {
    $settings['hide']['title'] = t('HIDE');
  }
}

